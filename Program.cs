﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace matice
{
    class Program
    {
        static List<float[,]> matice;

        static string zasobnik = "zasobnik.txt";
        static int pozice = 0;
        
        static void Main(string[] args)
        {
            PrintSeparator();
            Console.WriteLine("Interaktivní maticová kalkulačka v1.1");
            PrintSeparator();
            Console.WriteLine();
            NacistMatice();
            PrintHelp();
            PrintSeparator();
            while (true)
            {
                try {
                    OpSelect();
                } catch (Exception e) {
                    Console.WriteLine("!! Operace se nezdařila.");
                    Console.WriteLine("!! " + e.Message);
                }
                PrintSeparator();
            }
        }

        static void NacistMatice()
        {
            Console.Write("? Zadejte počet matic: ");
            int numMat = int.Parse(Console.ReadLine());
            matice = new List<float[,]>();
            PrintSeparator();
            for (int i = 0; i < numMat; i++)
            {
                Console.WriteLine("-> Matice č. " + (i+1).ToString());
                try { 
                    NovaMatice();
                } catch (Exception e) {
                    Console.WriteLine("!! " + e.Message);
                }
            }
        }

        static void PrintHelp()
        {
            PrintSeparator();
            Console.WriteLine(":: Možné operace:");
            Console.WriteLine("[H/HELP] Vytisknout nápovědu");
            Console.WriteLine("[N] Nová matice");
            if(matice.Count > 0)
            {
                Console.WriteLine("[E] Exportovat matici do souboru");
                Console.WriteLine("[P] Vytisknout matici");
                Console.WriteLine("[C] Vymazat matice");
                Console.WriteLine("[D] Determinant");
                Console.WriteLine("[I] Inverzní matice");
                Console.WriteLine("[T] Transponovat matici");
                Console.WriteLine("[+] Sečíst matice");
                Console.WriteLine("[x] Vynásobit matice");
                Console.WriteLine("[*] Vynásobit konstantou");
                Console.WriteLine("[S] Upravit do schodovitého tvaru");
            }
            Console.WriteLine("[Q] Ukončit aplikaci");
        }

        static void OpSelect()
        {
            Console.Write("? Vyberte operaci: ");
            var op = Console.ReadLine().ToLower();
            switch (op)
            {
                case "h":
                case "help":
                    PrintHelp();
                    return;
                case "n":
                    Console.WriteLine("-> Zadat novou matici");
                    NovaMatice();
                    return;
                case "q":
                    Console.WriteLine("-> Končím...");
                    Environment.Exit(0);
                    return;
            }
            if(matice.Count == 0)
            {
                Console.WriteLine(": Není zadána žádná matice. Možné operace jsou omezeny. Napište \"help\" pro vypsání možných operací.");
                return;
            }
            switch (op)
            {
                case "c":
                    matice.Clear();
                    Console.WriteLine("-> Matice vymazány");
                    break;
                case "e":
                    Console.WriteLine("-> Exportovat matici");
                    ExportMat(matice[VybratMatici()]);
                    break;
                case "p":
                    Vytisknout(matice[VybratMatici()]);
                    break;
                case "d":
                    var m = matice[VybratMatici()];
                    if (m.GetLength(0) != m.GetLength(1))
                    {
                        Console.WriteLine("!! Matice musí být čtvercová");
                        break;
                    }
                    Console.WriteLine("-> Determinant: " + Determinant(m).ToString());
                    break;
                case "i":
                    Console.WriteLine("-> Inverzní matice");
                    var inv = Inverzni(matice[VybratMatici()]);
                    Vytisknout(inv);
                    SaveMat(inv);
                    break;
                case "t":
                    Console.WriteLine("-> Transponovaná matice");
                    var t = Transponovat(matice[VybratMatici()]);
                    Vytisknout(t);
                    SaveMat(t);
                    break;
                case "+":
                    Console.WriteLine("-> První sčítaná matice:");
                    var m1 = matice[VybratMatici()];
                    Console.WriteLine("-> Druhá sčítaná matice:");
                    var m2 = matice[VybratMatici()];
                    if (m1.GetLength(0) == m2.GetLength(0) && m1.GetLength(1) == m2.GetLength(1))
                    {
                        var res = Secist(m1, m2);
                        Vytisknout(res);
                        SaveMat(res);
                    }
                    else Console.WriteLine("!! Nelze sčítat matice o různých rozměrech");
                    break;
                case "x":
                    Console.WriteLine("-> První násobená matice:");
                    m1 = matice[VybratMatici()];
                    Console.WriteLine("-> Druhá násobená matice:");
                    m2 = matice[VybratMatici()];
                    if (m1.GetLength(0) == m2.GetLength(1))
                    {
                        var res = Vynasobit(m1, m2);
                        Vytisknout(res);
                        SaveMat(res);
                    }
                    else Console.WriteLine("!! Tyto matice nelze vynásobit. První matice musí mít tolik sloupců jako druhá řádků.");
                    break;
                case "*":
                    m = matice[VybratMatici()];
                    Console.Write("? Vynásobit konstantou: ");
                    float coef; 
                    if(!float.TryParse(Console.ReadLine(), out coef))
                    {
                        Console.WriteLine("!! Bylo zadáno neplatné číslo. Rada: desetinná čísla se píšou s tečkou nebo čárkou podle systémové lokalizace");
                        break;
                    }
                    var res2 = Vynasobit(m, coef);
                    Vytisknout(res2);
                    SaveMat(res2);
                    break;
                case "s":
                    m = matice[VybratMatici()];
                    if (m.GetLength(0) != m.GetLength(1))
                    {
                        Console.WriteLine("!! Matice musí být čtvercová");
                        break;
                    }
                    res2 = SchodovityTvar(m);
                    Vytisknout(res2);
                    SaveMat(res2);
                    break;
                default:
                    Console.WriteLine("!! Neplatná operace. Napište \"help\" pro vypsání možných operací.");
                    break;
            }
        }

        static void NovaMatice()
        {
            Console.Write("? Načíst ze souboru (N) / Zadat ručně (R) / Vytvořit ze zásobníku čísel (Z): ");
            var res = Console.ReadLine().ToLower();
            if (res == "r")
            {
                matice.Add(Nacist());
                Console.WriteLine(": OK");
            }
            if (res == "n")
            {
                Console.Write("? Zadejte název souboru: ");
                var f = Console.ReadLine();
                matice.Add(NacistZeSouboru(f));
                Console.WriteLine(": OK");
            }
            if (res == "z")
            {
                matice.Add(VytvoritZeZasobniku());
                Console.WriteLine(": OK");
            }
        }

        static int VybratMatici()
        {
            if (matice.Count == 1) //Není na výběr - vybrat první matici
                return 0;
            
            Console.Write("? Vyberte číslo matice (1 - " + matice.Count + "): ");
            int num;
            if (int.TryParse(Console.ReadLine(), out num))
            {
                if(num > 0 && num <= matice.Count)
                    return num - 1;
            }
            Console.WriteLine("!! Zadejte prosím platné číslo");
            return VybratMatici();
        }

        static void ExportMat(float[,] mat)
        {
            Console.Write("? Zadejte název souboru k uložení: ");
            var file = Console.ReadLine();
            if (file == "" || File.Exists(file))
            {
                Console.WriteLine("!! Soubor již existuje");
                return;
            }
            StreamWriter sw = new StreamWriter(file);
            sw.WriteLine(mat.GetLength(0) + "x" + mat.GetLength(1));
            for (int y = 0; y < mat.GetLength(1); y++)
            {
                sw.Write(mat[0,y]);
                for (int x = 1; x < mat.GetLength(0); x++)
                {
                    sw.Write(" " + mat[x,y]);
                }
                sw.Write("\n");
            }
            sw.Close();
            Console.WriteLine("-> Matice uložena do souboru " + file);
        }

        static void SaveMat(float[,] mat)
        {
            Console.Write("? Přejete si tuto matici uložit jako novou matici? [A/n]: ");
            var ans = Console.ReadLine().ToLower();
            if (ans == "a" || ans == "")
            {
                matice.Add(mat);
                Console.WriteLine("-> Matice uložena jako číslo " + matice.Count);
            }
        }

        static float[,] SchodovityTvar(float[,] m)
        {
            var newM = (float[,])m.Clone();
            for (int x = 0; x < m.GetLength(0); x++)
            {
                var yNew = RadekKOdecteni(m, x);
                if (yNew != x)
                {
                    var oldTop = GetRow(newM, x);
                    var newTop = GetRow(newM, yNew);
                    ReplaceRow(ref newM, x, newTop);
                    ReplaceRow(ref newM, yNew, oldTop); 
                }
                for (int y = m.GetLength(1)-1; y >= x+1; y--)
                {
                    if (newM[x,y] == 0)
                        continue;
                    
                    var div = newM[x, y] / newM[x, x];
                    var mRow = MultRow(GetRow(newM, x), div);
                    var row = GetRow(newM, y);
                    var nRow = SubtRow(row, mRow);
                    ReplaceRow(ref newM, y, nRow);
                }
            }
            return newM;
        }

        static int RadekKOdecteni(float[,] m, int x)
        {
            for (int y = x; y < m.GetLength(1); y++)
            {
                if (m[x,y] != 0f)
                    return y;
            }
            Console.WriteLine("!! Nebyl nalezen vhodný řádek k odečtení. Matice nebude dávat smysl.");
            return x;
        }

        static float[] GetRow(float[,] m, int y)
        {
            return Enumerable.Range(0, m.GetLength(1))
                .Select(x => m[x, y])
                .ToArray();
        }

        static float[] MultRow(float[] row, float coef)
        {
            return row.Select(x => x * coef).ToArray();
        }

        static float[] SubtRow(float[] row1, float[] row2)
        {
            return Enumerable.Range(0, row1.Length).Select(i => row1[i] - row2[i]).ToArray();
        }

        static void ReplaceRow(ref float[,] m, int y, float[] row)
        {
            for(int x = 0; x < m.GetLength(0); x++)
            {
                m[x,y] = row[x];
            }
        }
 
        static float[,] Vynasobit(float[,] m1, float[,] m2)
        {
            var res = new float[m2.GetLength(0), m1.GetLength(1)];
            for (int x = 0; x < m2.GetLength(0); x++)
            {
                for (int y = 0; y < m1.GetLength(1); y++)
                {
                    float val = 0;
                    for (int p = 0; p < m1.GetLength(0); p++)
                    {
                        val += m1[p, y] * m2[x, p];
                    }
                    res[x,y] = val;
                }
            }
            return res;
        }

        static float[,] Vynasobit(float[,] m, float coef)
        {
            var res = new float[m.GetLength(0), m.GetLength(1)];
            for (int x = 0; x < m.GetLength(0); x++)
            {
                for (int y = 0; y < m.GetLength(1); y++)
                {
                    res[x,y] = m[x,y] * coef;
                }
            }
            return res;
        }
        
        static float[,] Secist(float[,] m1, float[,] m2)
        {
            var res = new float[m1.GetLength(0), m1.GetLength(1)];
            for (int x = 0; x < m1.GetLength(0); x++)
            {
                for (int y = 0; y < m1.GetLength(1); y++)
                {
                    res[x,y] = m1[x,y] + m2[x,y];
                }
            }
            return res;
        }

        static float[,] Transponovat(float[,] m)
        {
            var nm = new float[m.GetLength(1), m.GetLength(0)];
            for (int x = 0; x < m.GetLength(0); x++)
            {
                for (int y = 0; y < m.GetLength(1); y++)
                {
                    nm[y,x] = m[x,y];
                }
            }
            return nm;
        }

        static float[,] Inverzni(float[,] mat)
        {
            float[,] newMat = new float[mat.GetLength(0), mat.GetLength(1)];
            float det = Determinant(mat);
            if (det == 0)
            {
                Console.WriteLine("!! Inverzní matici nelze spočítat");
                return new float[0,0]; //Zero size matrix
            }
            for (int x = 0; x < mat.GetLength(0); x++)
            {
                for (int y = 0; y < mat.GetLength(1); y++)
                {
                    newMat[x,y] = (float)((Math.Pow(-1, x+y+2)*Determinant(RemoveElement(mat, y, x)))/det);
                }
            }
            return newMat;
        }

        static float Determinant(float[,] m)
        {
            if (m.GetLength(0) != m.GetLength(1))
            {
                Console.WriteLine("!! Determinant není definován");
                return 0;
            }

            if (m.GetLength(0) == 1)
                return m[0,0];

            if (m.GetLength(0) == 2)
                return Determinant2x2(m);
            
            //Podle prvního řádku
            float total = 0;
            for (int x = 0; x < m.GetLength(0); x++)
            {
                int coef = (int)Math.Pow(-1, 2+x);
                total += coef * m[x,0] * Determinant(RemoveElement(m, x, 0)); 
            }

            return total;
        }

        static float[,] RemoveElement(float[,] m, int ix, int iy)
        {
            float[,] newMat = new float[m.GetLength(0)-1, m.GetLength(1)-1];
            for (int x = 0; x < m.GetLength(0); x++)
            {
                if (x == ix)
                    continue;
                for (int y = 0; y < m.GetLength(1); y++)
                {
                    if (y == iy)
                        continue;
                    
                    int nx = x > ix ? x-1 : x;
                    int ny = y > iy ? y-1 : y;
                    newMat[nx, ny] = m[x,y];
                }
            }
            return newMat;
        }

        static float Determinant2x2(float[,] m)
        {
            return m[0,0]*m[1,1] - m[0,1]*m[1,0];
        }

        static float[,] Nacist()
        {
            Console.Write("? Zadejte velikost matice (např. 2x2): ");
            var dim = Console.ReadLine().Split('x');
            var sizeX = int.Parse(dim[0]);
            var sizeY = int.Parse(dim[1]);
            var mat = new float[sizeX, sizeY];

            Console.WriteLine("-> Zadejte matici (čísla oddělujte mezerami, řádek končí enterem)");
            for (int y = 0; y < sizeY; y++)
            {
                var line = Console.ReadLine().Split();
                for (int x = 0; x < sizeX; x++)
                    mat[x,y] = float.Parse(line[x]);
            }
            return mat;
        }

        static float[,] NacistZeSouboru(string path)
        {
            var sr = new StreamReader(path);
            var dim = sr.ReadLine().Split('x');
            var sizeX = int.Parse(dim[0]);
            var sizeY = int.Parse(dim[1]);
            var mat = new float[sizeX, sizeY];

            for (int y = 0; y < sizeY; y++)
            {
                var line = sr.ReadLine().Split();
                for (int x = 0; x < sizeX; x++)
                    mat[x,y] = float.Parse(line[x]);
            }
            return mat;
        }

        static float[,] VytvoritZeZasobniku()
        {
            Console.WriteLine(": Používám zásobník v souboru " + zasobnik + " Tento soubor musí být ve složce, ze které byl program spuštěn.");
            var sr = new StreamReader(zasobnik);
            float[] cisla = sr.ReadToEnd().Split((char[])null, StringSplitOptions.RemoveEmptyEntries)
                .Select(n => float.Parse(n)).ToArray();

            Console.Write("? Zadejte velikost matice (např. 2x2): ");
            var dim = Console.ReadLine().Split('x');
            var sizeX = int.Parse(dim[0]);
            var sizeY = int.Parse(dim[1]);
            var mat = new float[sizeX, sizeY];

            for (int y = 0; y < sizeY; y++)
            {
                for (int x = 0; x < sizeX; x++)
                {
                    if (pozice >= cisla.Length)
                        pozice = 0;
                    mat[x,y] = cisla[pozice];                    
                    pozice++;
                }
            }

            return mat;
        }

        static void Vytisknout(float[,] m)
        {
            var border = new String('-', m.GetLength(0)*7+1);
            Console.WriteLine(border);
            for (int y = 0; y < m.GetLength(1); y++)
            {
                string line = "|";
                for (int x = 0; x < m.GetLength(0); x++)
                    line += m[x,y].ToString("N3").TrimEnd('0').TrimEnd(',').PadLeft(6) + "|";
                Console.WriteLine(line);
                Console.WriteLine(border);
            }
        }
        
        static void PrintSeparator()
        {
            Console.WriteLine("--------------------------------");
        }
    }
}
