# INTERAKTIVNÍ MATICOVÁ KALKULAČKA

Umožňuje základní operace s maticemi.

Používá .NET Core 3.0

Spustit pomocí `dotnet run`

Vytvořeno na semináři programování.  
(C) 2020 Pavel Hronek